﻿using System.Collections;
using System.Collections.Generic;
using fromPhygitalism;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SphereController : MonoBehaviour, IClickable
{
    [SerializeField] private int nameCount;
    private Transform thisTransform;
    public PositionListModel positions;
    [SerializeField] [Range(0.0f, 1.0f)] private float speedInMSecond = 0.2f;
    private IEnumerator toMove;
    private int currentPoint = 0;
    private float radiusPoint = 0.01f;
    [SerializeField] [Range(5, 20)] private float speedSecond = 5;
    private Transform originPosition;
    private float LastSpeed;
    private float baseSpeed;
    


    public float SpeedInMSecond
    {
        get { return speedInMSecond; }
        set
        {
            LastSpeed = speedInMSecond;
            speedInMSecond = value;
        }
    }

    private void Start()
    {
        thisTransform = transform;
        LastSpeed = speedInMSecond;
        baseSpeed = speedInMSecond;
    }

    private void Update()
    {
        if (positions == null) return;
        if (toMove != null) return;
        toMove = ToMove();
        StartCoroutine(toMove);
    }


    IEnumerator ToMove()
    {
        thisTransform.position=Vector3.zero;
        LineRenderer line = GetComponent<LineRenderer>();
        line.positionCount = 0;
        while (currentPoint < positions.x.Length)
        {
            var newPoint = new Vector3(positions.x[currentPoint], positions.y[currentPoint], positions.z[currentPoint]);
            thisTransform.position =
                Vector3.MoveTowards(thisTransform.position,
                    newPoint,
                    Time.deltaTime * speedInMSecond * speedSecond);
            if (Vector3.Distance(newPoint, thisTransform.position) < radiusPoint)
            {
                line.positionCount += 1;
                line.SetPosition(currentPoint, thisTransform.position);

                currentPoint += 1;
            }

            yield return null;
        }

        StopCoroutine(toMove);
        positions = null;
        currentPoint = 0;
        toMove = null;
    }

    public void SetSpeed(float speed)
    {
        SpeedInMSecond = speed;
    }


    public GameObject SetList(PositionListModel positionList)
    {
        positions = positionList;
        return gameObject;
    }

    public int nameObject()
    {
        return nameCount;
    }

    public float GetSpeed()
    {
        return SpeedInMSecond;
    }

    public void SetColorSelected()
    {
        GetComponent<Renderer>().material.SetColor("_Color", Color.blue);
        speedInMSecond = LastSpeed;
    }

    public void SetColorUnselected()
    {
        GetComponent<Renderer>().material.SetColor("_Color", Color.white);       
    }

    public GameObject GetGameObject()
    {
        return gameObject;
    }


    private void OnMouseDown()
    {
        if (IsInvoking("DoubleClick"))
        {
            Debug.Log("double click");
            StopCoroutine(toMove);
            positions = null;
            currentPoint = 0;
            toMove = null;
            speedInMSecond = baseSpeed;
            thisTransform.position=Vector3.zero;
        }
        else
        {
            Invoke("DoubleClick", 0.5f);
        }
    }
}