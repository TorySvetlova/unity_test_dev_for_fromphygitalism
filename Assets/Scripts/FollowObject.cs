using UnityEngine;

namespace fromPhygitalism
{
    public class FollowObject : MonoBehaviour

    {
        private Transform thisTransform;
        private bool following;
        private Transform target;
        [SerializeField] private float speedMotion;

        private void Start()
        {
            thisTransform = transform;
            SceneController.Instance.setTarget += setTarget;
        }

        private void setTarget(GameObject obj)
        {
            target = obj.transform;
            following = true;
        }

        private void LateUpdate()
        {
            if (following)
            {
                thisTransform.position =
                    Vector3.Lerp(thisTransform.position, target.position, Time.deltaTime * speedMotion);
            }
        }
    }
}