﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace fromPhygitalism
{
    public class MouseLook : MonoBehaviour
    {
        [SerializeField] private float xsen, ysen;
        [SerializeField] private float maxPitch;
        [SerializeField] private float maxYaw ;
        [SerializeField] private Transform target;

        private float pitch;
        private float yaw;

        void Update()
        {
            if (!Input.GetButton("Fire2")) return;
            var curEuler = target.localRotation.eulerAngles;
            curEuler = new Vector3(pitch - Input.GetAxis("Mouse Y"), yaw + Input.GetAxis("Mouse X"), 0);
            curEuler.z = 0;
            curEuler.x = Mathf.Clamp(curEuler.x, -maxPitch, maxPitch / 2);
            curEuler.y = Mathf.Clamp(curEuler.y, -maxYaw, maxYaw);
            pitch = curEuler.x;
            yaw = curEuler.y;

            target.localRotation = Quaternion.Euler(curEuler);
        }
    }
}