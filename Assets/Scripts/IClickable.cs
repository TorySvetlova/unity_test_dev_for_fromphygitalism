using UnityEngine;
using UnityEngine.UI;

namespace fromPhygitalism
{
    public interface IClickable
    {
        GameObject SetList(PositionListModel positionList);
        int nameObject();
        float GetSpeed();
        void SetSpeed(float speed);
        void SetColorSelected();
        void SetColorUnselected();
        GameObject GetGameObject();
    }
}