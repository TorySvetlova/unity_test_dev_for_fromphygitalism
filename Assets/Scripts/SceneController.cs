﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UIElements;
using Slider = UnityEngine.UI.Slider;

namespace fromPhygitalism
{
    public class SceneController : MonoBehaviour
    {
        public static SceneController Instance;
        private Camera cam;
        private List<IClickable> spheres = new List<IClickable>();
        public Action<GameObject> setTarget;
        private int currentSphere;
        private GameObject go;
        [SerializeField] private string _name = "ball_path";
        [SerializeField] private Slider _slider;


        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            else
            {
                Destroy(gameObject);
            }
        }

        private void Start()
        {
            var sphereses = (SphereController[]) FindObjectsOfType(typeof(SphereController));
            foreach (var item in sphereses)
            {
                spheres.Add(item);
            }

            cam = Camera.main;
            go = spheres[0].GetGameObject();
        }

        private void Update()
        {
            if (Input.GetButtonDown("Fire1"))
            {
                SettingSphere();
            }
        }

        void SettingSphere()
        {
            var hit = Raycasting();
            if (hit.collider == null || hit.collider.GetComponent<IClickable>() == null) return;
            var thisSphere = hit.collider.GetComponent<IClickable>();
            if (setTarget != null) setTarget.Invoke(thisSphere.SetList(LoadJsonData(thisSphere.nameObject())));
            selectUnselected(hit);
        }

        void selectUnselected(RaycastHit hit)
        {
            go.GetComponent<IClickable>().SetSpeed(0);
            go.GetComponent<IClickable>().SetColorUnselected();
            go = hit.collider.gameObject;
            go.GetComponent<IClickable>().SetColorSelected();
            _slider.value = go.GetComponent<IClickable>().GetSpeed();
        }

        private PositionListModel LoadJsonData(int count)
        {
            var newList = new PositionListModel();
            var resorces = Resources.Load<TextAsset>(string.Format("{0}{1}", _name, count));
            newList = JsonUtility.FromJson<PositionListModel>(resorces.text);
            return newList;
        }

        private RaycastHit Raycasting()
        {
            RaycastHit hit;
            Physics.Raycast(cam.ScreenPointToRay(Input.mousePosition), out hit);
            return hit;
        }

        public void setSphere(bool direction)
        {
            if (!direction)
            {             
                currentSphere += 1;
                if (currentSphere >= spheres.Count) currentSphere = 0;
            }

            if (direction)
            {             
                currentSphere -= 1;
                if (currentSphere < 0) currentSphere = spheres.Count - 1;
            }

            go.GetComponent<IClickable>().SetSpeed(0);
            go.GetComponent<IClickable>().SetColorUnselected();
            go = spheres[currentSphere].GetGameObject();
            go.GetComponent<IClickable>().SetColorSelected();
            _slider.value = go.GetComponent<IClickable>().GetSpeed();
            setTarget.Invoke(spheres[currentSphere].GetGameObject());
        }

        public void SetSpeed(Slider slider)
        {
            go.GetComponent<IClickable>().SetSpeed(slider.value);
        }
    }
}